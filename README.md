# Acrylic Sheep Pack

1.15 Vanilla resource pack combing sublte features from Vanilla Tweaks, Pastelic Pack, and Plast Pack

Lag optimized and compatible with Optifine!

![](panorama.png)

# Features
(Environment)
- Clear Water*
- Galaxy Sky*
- Quieter Rain

(PVP)
- Lower Fire and Shield models
- Bow/Crossbow Charge Texture
- Enhanced critical hit particles
- Night Vision*
- Transparent GUI
- Removed obstrusive pumpkin mask


(Convenience)
- Night Vision
- Night mode loading screen
- Quieter rain
- Removed obstrusive pumpkin mask
- Unobtrusive Glass
- Connected bookshelves textures
- Connected glass textures*
- Helpful 3D Noteblock Models for musicians
- Different Models for Pumpkin and Melon stems
- Quieter Minecarts, Villagers, Nether Portals, and Endermen
- Flower indicator for max grown kelp crops
- Subtle texture indicator for direction of hopper
- Better Particles (Colorful enchants, Critical hit particles)
- Anti-cheese lava
- Cute Piglins

Features marked with * require Optifine.

# Sources
- [Vanilla Tweaks](https://vanillatweaks.net/picker/resource-packs/)
- [Pastelic Pack](https://www.youtube.com/watch?v=6cyeuOWgGNU)
- [Plast Pack](https://github.com/Plastix/Plast-Pack)
- [Night Vision](https://www.planetminecraft.com/texture_pack/night-vision-4444593/download/file/12542833/)
- [3D Noteblock Displays](https://www.creatorlabs.net/downloads/3d-noteblock-displays/)
- [Cute PIglins](https://www.planetminecraft.com/texture-pack/piglin-chan-4662204/)
